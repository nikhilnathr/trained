var currentPage = getPageNameFromURL();
resetRipples();
var stopsList = [];

function getStopsList() {
  return new Promise ((resolve) => {
    fetch("./api/stops.php")
      .then((res) => {return res.json();})
      .then((vals) => {
        if (vals.error) {
          console.log("error in retriving all station information");
          return;
        }
        vals.forEach( function(val) {
          stopsList.push({ id: val.stop_id, name: val.stop_name });
        })
        resolve();
      })
  })
}

document.addEventListener('DOMContentLoaded', function() {
  if (stopsList.length == 0) {
    getStopsList()
      .then( function() {
        loading_complete(currentPage);
        intiate(currentPage);
      })
  }
})

function searchStation() {
  let currentStation = document.querySelector(".search .station-select").value;

  currentStation = stopsList.map(stop => stop.name).includesi(currentStation);
  if (currentStation != false) {
    // search using pjax
    requestPage("search-results.html?id=" + stopsList[currentStation].id);
  } else {
    console.log("invalid station name given to search");
  }

}

function populateSuggestions() {
  searchQuery = this.value;
  fetch("./api/stops.php?search="+searchQuery)
    .then(res => res.json())
    .then(vals => {
      document.querySelector(".suggestions").innerHTML = "";
      if (vals.error) {
        console.log(vals);
        return;
      }
      vals.forEach((val) => {
        insertTo(".suggestions", val)
      })
    })
}

function insertTo(querySel, elem) {
  queryElem = document.querySelector(querySel);
  let searchElem = document.createElement("DIV");
  searchElem.classList.add("stop-search-result");
  searchElem.id = elem.stop_id;
  searchElem.innerHTML = elem.stop_name;
  searchElem.addEventListener("click", function() {
    document.querySelector(".search .station-select").value = elem.stop_name;
    queryElem.style.opacity = 0;
    setTimeout( function() {
      queryElem.innerHTML = "";
    }, 500);
  })
  queryElem.appendChild(searchElem);

}
