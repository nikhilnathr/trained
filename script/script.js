var visitedPages = [];


Date.prototype.weekday = function() {
  return (this.getDay() == 0) ? "su" : "wk";
}
Array.prototype.includesi = function(para) {
  var isFound = false;
  this.forEach( function(stopName, pos) {
    if (stopName.toUpperCase() == para.toUpperCase()) {
      isFound = stopName;
    }
  })
  if (isFound != false) return this.indexOf(isFound);
  return isFound;
}

function getPageNameFromURL() {
  let lP = window.location.href;
  pageName = "./" + lP.split("ed/")[1].split("?")[0];

  pages = {
    "./": "index",
    "./index.html": "index",
    "./search-results.html": "search-results"
  }

  return pages[pageName];
}

function intiate(pageName) {
  let container = document.querySelector(".container");
  container.className = "container";

  if (pageName === "index") {
    // add the respective container class
    container.classList.add("index");

    var stationSelect = document.querySelector(".search .station-select");

    stationSelect.addEventListener("keyup", populateSuggestions);

    document.querySelector(".search-button").addEventListener("click", searchStation);

  } else if (pageName === "search-results") {
    // add the respective container class
    container.classList.add("search-results");

    var searchResultsName = (new URL(window.location.href)).searchParams.get("id");

    let stopName = document.querySelector(".stop-name");

    let currentDatetime = new Date();

    fetch("./api/times.php?stop_id="+ searchResultsName +"&week="+ currentDatetime.weekday() +"&hrs="+currentDatetime.getHours())
      .then((res) => {return res.json();})
      .then((data) => {
        if (data.error) {
          // show the error
          stopName.innerHTML = "Please select the stop name from the dropdown list";     // just the sample error
          return;
        }
        // show stop name as heading
        stopName.innerHTML = searchResultsName;

        // show the trains according to the correct direction
        showTrainTimings(data);
      })
  }
}

function resetRipples() {
  for (i=0; el = document.querySelectorAll(".ripple-click")[i]; i++) {
    el.addEventListener("click", function(e) {
      let rippleContainer = document.createElement("DIV");
      rippleContainer.classList.add("ripple-container");
      this.appendChild(rippleContainer);

      let rippleDuration = "500ms";
      if (this.getAttribute('ripple-duration')) {
        rippleDuration = this.getAttribute('ripple-duration');
      }
      let rippleColor = "#000000";
      if (this.getAttribute('ripple-color')) {
        rippleColor = this.getAttribute('ripple-color');
      }

      rippleContainer.style.height = getComputedStyle(this).getPropertyValue('height');
      rippleContainer.style.width = getComputedStyle(this).getPropertyValue('width');
      rippleContainer.style.borderRadius = getComputedStyle(this).getPropertyValue('border-radius');

      let ripple = document.createElement("DIV");
      ripple.classList.add("ripple");
      ripple.style.animation = "ripple-grow "+ rippleDuration +" ease-in forwards";
      ripple.style.backgroundColor = rippleColor;
      rippleContainer.appendChild(ripple);
      let animDuration = getComputedStyle(ripple).getPropertyValue('animation-duration');
      animDuration = animDuration.substr(0, animDuration.length - 1);
      setTimeout(() => {
        rippleContainer.parentElement.removeChild(rippleContainer);
      }, animDuration * 1000)
    });
  }
}

window.addEventListener('popstate', function(e) {
  e.preventDefault();
  lastPage = visitedPages.pop();
  if ( visitedPages.length < 1) {
    requestPage(false);
  } else {
    requestPage(lastPage);
  }

});

function requestPage(pageName) {

  let lP = window.location.href;
  visitedPages.push(lP);
  if (pageName == false) {
    console.log("no page name given");
    let lP = window.location.href;
    visitedPages.push(lP);
    pageName = "./" + lP.substr(lP.charAt(lP.indexOf(".me/")+3), lP.length - lP.charAt(lP.indexOf(".me/")+3));
  }

  history.pushState(null, null, pageName);

  var myHeaders = new Headers();
  myHeaders.append('x-pjax', 'yes');

  loading_start()
  .then(function() {
    fetch(pageName, {
      method: 'GET',
      headers: myHeaders,
    }).then(function(response) {
      return response.text();
    }).then(function(text) {
      let tempElem = document.createElement("DIV");
      tempElem.innerHTML = text;

      let newContent  = tempElem.querySelector(".container").innerHTML;
      document.querySelector(".container").innerHTML = newContent;
      currentPage = getPageNameFromURL();
      resetRipples();
      intiate(currentPage);
      loading_complete(currentPage);
    });
  });

}

function animateSearchStationElements() {
  let searchIcon = document.querySelector(".search-icon"),
      stationSelect = document.querySelector(".station-select"),
      searchButton = document.querySelector(".search-button"),
      searchBox = document.querySelector(".search-box");

  searchBox.style.transform = "scale(1)";
  searchButton.style.backgroundColor = "#FFFFFF";
  setTimeout (() => {
    searchIcon.classList.add("start");
  }, 500);
}

function loading_start() {
  let logoPath = document.getElementById("path829"),
      dividerPath = document.getElementById("path007"),
      logoPathAnimationDuration = getComputedStyle(logoPath).getPropertyValue('animation-duration'),
      currentoffset = getComputedStyle(logoPath).getPropertyValue('stroke-dashoffset'),
      logoPathStrokeDashoffset = 1600;

  logoPathAnimationDuration = logoPathAnimationDuration.substr(0, logoPathAnimationDuration.length - 1);
  oneOffsetTime = logoPathAnimationDuration/logoPathStrokeDashoffset;

  dividerPath.style.animation = "none";
  document.querySelector(".loading").style.display = "flex";

  return new Promise((resolve) => {
    setTimeout( function() {
      document.querySelector(".loading .cover.up").style.top = "0";
      document.querySelector(".loading .cover.down").style.top = "50vh";

      setTimeout( function() {
        logoPath.style.animation = "dash 1.5s ease-in infinite";
        resolve();
      }, 500)
    }, 100);
  })

}

function loading_complete(page) {
  let logoPath = document.getElementById("path829"),
      dividerPath = document.getElementById("path007"),
      logoPathAnimationDuration = getComputedStyle(logoPath).getPropertyValue('animation-duration'),
      currentoffset = getComputedStyle(logoPath).getPropertyValue('stroke-dashoffset'),
      logoPathStrokeDashoffset = 1600;

  logoPathAnimationDuration = logoPathAnimationDuration.substr(0, logoPathAnimationDuration.length - 1);
  oneOffsetTime = logoPathAnimationDuration/logoPathStrokeDashoffset;

  currentoffset = currentoffset.substr(0, currentoffset.length - 2);
  currentDelay = currentoffset * oneOffsetTime * 1000 - 100;

  setTimeout( function() {
    logoPath.style.animation = "none";
    document.querySelector(".loading .divider").style.display = "block";
    dividerPath.style.animation = "dash-2 1s ease-in forwards";
    setTimeout ( function() {
      document.querySelector(".loading .cover.up").style.top = "-50vh";
      document.querySelector(".loading .cover.down").style.top = "100vh";
      if (page == "index") {
        animateSearchStationElements();
      }
      setTimeout( function() {
        document.querySelector(".loading").style.display = "none";
      }, 500)
    }, 500);
  }, currentDelay);
}
