<?php
require_once('csv_to_string.php');

class Fare {
  public $fares;
  function __construct() {
    $path    = 'KMRL/fare_attributes.txt';          // fare attributes from Kochi metro open data https://kochimetro.org/open-data
    $this->fares = csv_to_string($path);
  }

  // RETURN FARE FROM FARE ID
  function fare_from_id ($fare_id) {
    foreach ($this->fares as $fare) {
      if (strcasecmp($fare['fare_id'], $fare_id) === 0) {
        return $fare['price'];
      }
    }
    return false;
  }

  // GET CURRENCY VALUE
  function get_currency () {
    return $this->fares[0]['currency_type'];
  }
}
