<?php
require_once('csv_to_string.php');

class Stop {
  public $stops;
  function __construct() {
    $path    = 'KMRL/stops.txt';          // stops list from Kochi metro open data https://kochimetro.org/open-data
    $this->stops = csv_to_string($path);

    // To get translation of various stops
    $path    = 'KMRL/translations.txt';          // translations list from Kochi metro open data https://kochimetro.org/open-data/
    $content = csv_to_string($path);

    for($i = 0; $i < sizeof($content) - 1; $i++) {
      $this->stops[$i]['translations']['ml'] = $content[$i]['translation'];
    }
  }

  // RETURN INFO ABOUT ALL STOPS
  function get_all_stops() {
    return json_encode($this->stops);
  }

  // RETURN ALL INFO ABOUT A PARTICULAR STOP
  function info_from_stop_id($stop_id) {
    foreach ($this->stops as $stop) {
      if (strcasecmp($stop['stop_id'], $stop_id) === 0) {
        return json_encode($stop);
      }
    }
    return false;
  }

  // SEARCH RELATED QUERY RESULTS
  function search($query) {
    $search_weightage = array();
    foreach ($this->stops as $stop) {
      if (stripos($stop['stop_id'], $query) !== false) {
        $weightage = -count($stop) + stripos($stop['stop_name'], $query);
        $stop['weightage'] = $weightage;
        array_push($search_weightage, $stop);
      } else if (stripos($stop['stop_name'], $query) !== false) {
        $weightage = stripos($stop['stop_name'], $query);
        $stop['weightage'] = $weightage;
        array_push($search_weightage, $stop);
      }
    }

    // sort the array with weightage
    for ($i = 0; $i < sizeof($search_weightage)-1; $i++) {
      $sml_elem = $search_weightage[$i];
      $small_elem_pos = $i;
      for ($j = $i+1; $j < sizeof($search_weightage); $j++) {
        if ($sml_elem['weightage'] > $search_weightage[$j]['weightage']) {
          $sml_elem = $search_weightage[$j];
          $small_elem_pos = $j;
        }
      }
      // swap the elems
      $temp = $search_weightage[$i];
      $search_weightage[$i] = $search_weightage[$small_elem_pos];
      $search_weightage[$small_elem_pos] = $temp;
    }

    if (sizeof($search_weightage) < 1) {
      return false;
    } else {
      return json_encode($search_weightage);
    }
  }

  // RETURN STOP NAME FROM STOP ID
  function name_from_id ($stop_id) {
    foreach ($this->stops as $stop) {
      if (strcasecmp($stop['stop_id'], $stop_id) === 0) {
        return $stop['stop_name'];
      }
    }
    return false;
  }

  // RETURN STOP ID FROM STOP NAME
  function id_from_name ($stop_name) {
    foreach ($this->stops as $stop) {
      if (strcasecmp($stop['stop_name'], $stop_name) === 0) {
        return $stop['stop_id'];
      }
    }
    return false;
  }

  // RETURN STOP NAME FROM SERIAL ORDER
  function stop_from_num ($order) {
    if (is_int($order) && $order > 0 && $order <= sizeof($this->stops)) {
      return $this->stops[$order-1]['stop_name'];
    }
    return false;
  }
}
