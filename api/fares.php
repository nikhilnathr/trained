<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once('class/Fare.php');
require_once('csv_to_string.php');
require_once('class/Stop.php');
$path    = 'KMRL/fare_rules.txt';          // fare rules from Kochi metro open data https://kochimetro.org/open-data/
$fare_rules = csv_to_string($path);

// TODO: Change this to a FARES OBJECT

$stop = new Stop;
$fare = new Fare;

if (count($_GET) === 0) {
  // NO STATION SPECIFIED RETURN PRICE FOR ALL STATIONS
  for ($i = 0; $i < sizeof($fare_rules); $i++) {
    $fare_rules[$i]['origin'] = $stop->name_from_id($fare_rules[$i]['origin_id']);
    $fare_rules[$i]['destination'] = $stop->name_from_id($fare_rules[$i]['destination_id']);
    $fare_rules[$i]['price'] = $fare->fare_from_id($fare_rules[$i]['fare_id']);
    $fare_rules[$i]['currency'] = $fare->get_currency();
    unset($fare_rules[$i]['fare_id']);
  }
  http_response_code(200);
  echo json_encode($fare_rules);
}

// GET PRICES FROM A PARTICULAR STTAION
else if (count($_GET) === 1 && isset($_GET['from'])) {
  $from = strtoupper($_GET['from']);
  
  if ($stop->name_from_id($from)) {
    $fare_rule = array();
    $custom_fare_rule = array();
    for ($i = 0; $i < sizeof($fare_rules); $i++) {
      if ($from != $fare_rules[$i]['origin_id']) {
        continue;
      }
      $fare_rule['origin_id'] = $fare_rules[$i]['origin_id'];
      $fare_rule['destination_id'] = $fare_rules[$i]['destination_id'];
      $fare_rule['origin'] = $stop->name_from_id($fare_rules[$i]['origin_id']);
      $fare_rule['destination'] = $stop->name_from_id($fare_rules[$i]['destination_id']);
      $fare_rule['price'] = $fare->fare_from_id($fare_rules[$i]['fare_id']);
      $fare_rule['currency'] = $fare->get_currency();
      array_push($custom_fare_rule, $fare_rule);
    }
    http_response_code(200);
    echo json_encode($custom_fare_rule);
  } else {
    // the station code is invalid
    http_response_code(404);
    echo json_encode(array("error" => "Invalid stop id"));
  }
  
}

// GET THE PRICE BETWEEN TWO STATIONS
else if (count($_GET) === 2 && isset($_GET['from']) && isset($_GET['to'])) {
  $from = strtoupper($_GET['from']);
  $to = strtoupper($_GET['to']);
  
  if ($stop->name_from_id($from) && $stop->name_from_id($to)) {
    $fare_rule = array();
    for ($i = 0; $i < sizeof($fare_rules); $i++) {
      if ($from != $fare_rules[$i]['origin_id'] || $to != $fare_rules[$i]['destination_id']) {
        continue;
      }
      $fare_rule['origin_id'] = $fare_rules[$i]['origin_id'];
      $fare_rule['destination_id'] = $fare_rules[$i]['destination_id'];
      $fare_rule['origin'] = $stop->name_from_id($fare_rules[$i]['origin_id']);
      $fare_rule['destination'] = $stop->name_from_id($fare_rules[$i]['destination_id']);
      $fare_rule['price'] = $fare->fare_from_id($fare_rules[$i]['fare_id']);
      $fare_rule['currency'] = $fare->get_currency();
    }
    http_response_code(200);
    echo json_encode($fare_rule);
  } else {
    // the station code is invalid
    http_response_code(404);
    echo json_encode(array("error" => "Invalid stop id"));
  }
}

// INVALID PARAMETERS
else {
  http_response_code(400);
  echo json_encode(array("error" => "Invalid parameters"));
}
