<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once('class/Stop.php');

$stop = new Stop;

if (sizeof($_GET) === 0) {
  http_response_code(200);
  echo $stop->get_all_stops();
}

// RETURN VALUES OF A SPECIFIC CODE
else if (sizeof($_GET) === 1 && isset($_GET['stop_id'])) {
  $stop_id = htmlspecialchars($_GET['stop_id']);

  $info = $stop->info_from_stop_id($stop_id);

  if ($info === false) {
    http_response_code(404);
    echo json_encode(array("error" => "Invalid stop id"));
  } else {
    http_response_code(200);
    echo $info;
  }
}

// SEARCH WITH A PARAMETER
else if (sizeof($_GET) === 1 && isset($_GET['search'])) {
  $search = htmlspecialchars($_GET['search']);

  $search_results = $stop->search($search);

  if ($search_results === false) {
    http_response_code(404);
    echo json_encode(array("error" => "No results found"));
  } else {
    http_response_code(200);
    echo $search_results;
  }
}

else {
  http_response_code(400);
  echo json_encode(array("error" => "Invalid parameters"));
}
