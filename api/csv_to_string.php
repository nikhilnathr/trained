<?php
function csv_to_string($path) {
  $plain_contents = file_get_contents($path);
  $plain_contents = str_replace("\r", "", $plain_contents);
  $contents = explode("\n", $plain_contents);
  array_pop($contents);
  $csv = array();
  $headings = explode(',', $contents[0]);
  for($i = 1; $i < sizeof($contents); $i++) {
    $row_data = explode(',', $contents[$i]);
    for($j = 0; $j < sizeof($row_data); $j++) {
      $csv[$i-1][$headings[$j]] = $row_data[$j];
    }
  }

  return $csv;
}
