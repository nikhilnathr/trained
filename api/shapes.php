<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once('csv_to_string.php');

// TODO: Change this to a SHAPE OBJECT

$path = 'KMRL/shapes.txt';          // shapes from Kochi metro open data https://kochimetro.org/open-data/
$shapes = csv_to_string($path);

$formatted = $temp = $new_id = array();
for ($i = 0; $i < sizeof($shapes); $i++) {
  $shape_id = $shapes[$i]['shape_id'];
  
  // add necessary data to be inserted to new array
  $temp['sl'] = $shapes[$i]['shape_pt_sequence'];
  $temp['lat'] = $shapes[$i]['shape_pt_lat'];
  $temp['lon'] = $shapes[$i]['shape_pt_lon'];
  $temp['dist'] = $shapes[$i]['shape_dist_traveled'];

  // get shape ids array and get position of current point's shape id
  $cur_ids = array_map(function($points) { return $points['shape_id']; }, $formatted);
  $array_pos = array_search($shape_id, $cur_ids);
  
  if ($array_pos !== false) {
    array_push($formatted[$array_pos]['points'], $temp);
  } 
  // if shape id not present create and insert
  else {
    $new_id = array("shape_id"=>$shape_id, "points" => array($temp));
    array_push($formatted, $new_id);
  }
}

// IF NO PARAMETER SPECIFIED
if (count($_GET) == 0) {
  http_response_code(200);
  echo json_encode($formatted);
}

// RETURN VALUES OF A SPECIFIC SHAPE ID
else if (sizeof($_GET) === 1 && isset($_GET['shape_id'])) {
  $shape_id = htmlspecialchars($_GET['shape_id']);
  $cur_ids = array_map(function($points) { return $points['shape_id']; }, $formatted);
  $array_pos = array_search($shape_id, $cur_ids);
  
  if ($array_pos !== false) {
    http_response_code(200);
    echo json_encode($formatted[$array_pos]);
  }

  else {
    http_response_code(404);
    echo json_encode(array("error" => "Invalid shape id"));
  }
}

// STRUCTURE OF QUERY INVALID
else {
  http_response_code(400);
  echo json_encode(array("error" => "Invalid parameters"));
}
