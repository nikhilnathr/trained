<?php
// required headers
header("Access-Control-Allow-Origin: http://localhost");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once('csv_to_string.php');
require_once('class/Stop.php');

$stop = new Stop;
$path = 'KMRL/stop_times.txt';          // stop times from Kochi metro open data https://kochimetro.org/open-data/
$times = csv_to_string($path);

if (count($_GET) === 0) {
  http_response_code(200);
  echo json_encode($times);
}

// TIMINGS AND DIFFERENT STATIONS IN THE TRIP
else if (count($_GET) === 1 && isset($_GET['trip_id'])) {
  $current = array();
  $trip_id = htmlspecialchars($_GET['trip_id']);
  foreach($times as $time) {
    if(strcasecmp($time['trip_id'], $trip_id) === 0) {
      array_push($current, $time);
    }
  }

  if (count($current) > 0) {
    http_response_code(200);
    echo json_encode($current);
  } else {
    http_response_code(404);
    echo json_encode(array("error" => "Invalid trip id"));
  }
}

  // GET ARRIVAL AND DEPARTURES FROM A PARTICULAR STATION
if (count($_GET) > 0 && count($_GET) <= 4 && isset($_GET['stop_id']) ) {
  $current = array();

  if ($stop->name_from_id($_GET['stop_id'])) {
    // get day of the week if specified
    $day_of_week = (isset($_GET['week']))?$_GET['week']:"";     // wk = weekdays, su = sunday

    if ($day_of_week != "wk" && $day_of_week != "su" && $day_of_week != "") {
      http_response_code(404);
      echo json_encode(array("error" => "Invalid day"));
      exit;
    }

    // get travelling directions
    $direction = (isset($_GET['direction'])) ? $day_of_week.$_GET['direction'] : "";     // r = alva ro mace, l = mace to aluva
    if ($direction != $day_of_week."r" && $direction != $day_of_week."l" && $direction != "") {
      http_response_code(404);
      echo json_encode(array("error" => "Invalid direction"));
      exit;
    }

    $hrs = (isset($_GET['hrs']))?$_GET['hrs']:"";
    if ($hrs != "" && ($hrs > 24 || $hrs < 1)) {
      http_response_code(404);
      echo json_encode(array("error" => "Invalid time"));
      exit;
    }

    ($direction == "")? $direction = "r": null;
    ($day_of_week == "")? $day_of_week = "r": null;
    ($hrs == "")? $time_is_set = false: $time_is_set = true;
    foreach($times as $time) {
      $arrival_time_hrs = explode(":", $time['arrival_time'])[0];
      if(strcasecmp($time['stop_id'], $_GET['stop_id']) === 0 &&
          strpos($time['trip_id'], strtoupper($day_of_week)) !== false &&
          strpos($time['trip_id'], strtoupper($direction)) !== false &&
          (!$time_is_set || ($arrival_time_hrs == $hrs)) ) {
        array_push($current, $time);
      }
    }
    http_response_code(200);
    echo json_encode($current);
  } else {
    // the station code is invalid
    http_response_code(404);
    echo json_encode(array("error" => "Invalid stop id"));
  }
    }

else {
  http_response_code(400);
  echo json_encode(array("error" => "Invalid parameters"));
}
