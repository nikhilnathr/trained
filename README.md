
# Trained

  

The API and front-end for the **Kochi Metro Rail Limited** ([KMRL](http://kochimetro.org)) open data. *Contains data provided by Kochi Metro Rail Limited*. The data provided by KMRL is in **GTFS-static** format, as JSON is more easier to handle than GTFS format this API converts all the data to **JSON** and also support queries based many other parameters.

The API is written in PHP and was developed in **`PHP`** version `7.2.12`. The front-end for this API is just an example written in pure JavaScript, CSS and HTML with no frameworks.